﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewLife.Cube.Common
{
    public enum FormatType
    {
        /// <summary>
        /// 小驼峰，
        /// </summary>
        CamelCase = 0,

        /// <summary>
        /// 小写
        /// </summary>
        LowerCase = 1,

        /// <summary>
        /// 保持默认
        /// </summary>
        DefaultCase = 2
    }
}
